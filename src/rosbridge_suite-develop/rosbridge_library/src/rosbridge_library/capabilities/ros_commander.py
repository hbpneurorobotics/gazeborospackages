"""
Executes ros commands
"""

import os
import time

from twisted.internet import protocol, defer
from twisted.internet import reactor, utils, error

from rosbridge_library.capability import Capability

ROS_COMPLETER_SCRIPT_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                         'roscommand_completer.sh')

ROS_DISTRO, ROS_ROOT = os.environ["ROS_DISTRO"], os.environ["ROS_ROOT"]

# directory where ROS commands are stored: default is /opt/ros/$ROS_DISTRO/bin
ROS_BIN_DIR = os.path.join(ROS_ROOT[:ROS_ROOT.find(ROS_DISTRO)],
                           ROS_DISTRO,
                           "bin")
LOGGER_LEVEL = "debug"


class ExecuteRosCommand(Capability):
    """
    Executes ROS commands
    """

    MSG_FIELDS = [(True, "msg", dict)]

    VALID_COMMANDS = ['help', 'rostopic', 'rosservice', 'rosmsg', 'rossrv', 'rosnode']
    VALID_COMMANDS_LOOKUP = dict.fromkeys(VALID_COMMANDS, "execute_cmd")

    INTERNAL_COMMANDS_LOOKUP = {"COMPLETE": "complete_cmd",
                                "STOP_CURRENT_EXECUTION": "stop_execution_cmd"}

    # Map cmd -> function_cmd.
    # function_cmd is an instance method of this class (ExecuteRosCommand)
    CMDs_LOOKUP = dict(**VALID_COMMANDS_LOOKUP)
    CMDs_LOOKUP.update(INTERNAL_COMMANDS_LOOKUP)

    def __init__(self, proto):
        # Call superclass constructor
        Capability.__init__(self, proto)

        self.current_process_transport = None

        # Register the operations that this capability provides
        self.protocol.register_operation("roscmd", self.execute_ros_command)

    def finish(self):
        """
        Dispose Commander resources
        """
        self.protocol.log(LOGGER_LEVEL, "finish")
        self.stop_execution_cmd()

    def execute_ros_command(self, message):
        """
        {"op": "roscmd"} messages handler
        message arg format: "msg":{"cmd":"<ROS_COMMAND>","args":[<ARGS>]}}
        """
        self.protocol.log(LOGGER_LEVEL, "execute_ros_command - "
                                        "Message %s" % message)

        # Check the args
        self.basic_type_check(message, ExecuteRosCommand.MSG_FIELDS)

        msg = message['msg']

        cmd = msg.get('cmd', None)
        args = msg.get('args', None)

        # if cmd is invalid (i.e. not in the dict), self.invalid_cmd will be called
        cmd_handler = self.CMDs_LOOKUP.get(cmd, "invalid_cmd")

        # call handler
        getattr(self, cmd_handler)(cmd, args)

    def send_ros_response(self, data=None, running=True):
        self.protocol.log(LOGGER_LEVEL, "send_ros_response - "
                                        "Data %s" % data)

        self.protocol.send({'op': 'ros_response',
                            'msg': {'data': data},
                            'running': running})

    def invalid_cmd(self, cmd, _args):
        self.protocol.log(LOGGER_LEVEL, "invalid_cmd - "
                                        "cmd is %s" % cmd)

        self.send_ros_response(data=[{'data': 'Invalid command: %s' % cmd, 'type': 'stderr'}],
                               running=False)

    def stop_execution_cmd(self, _cmd=None, _args=None):

        if self.current_process_transport:
            try:
                self.current_process_transport.signalProcess('KILL')
            except error.ProcessExitedAlready:
                self.protocol.log(LOGGER_LEVEL, "stop_execution_cmd - "
                                                "ProcessExitedAlready")
            finally:
                self.protocol.log(LOGGER_LEVEL, "stop_execution_cmd - "
                                                "Sent KILL to child.")
        else:
            self.protocol.log(LOGGER_LEVEL,
                              "stop_execution_cmd - "
                              "Process not found (died or never started), "
                              "current_process_transport is None")

    @defer.inlineCallbacks
    def complete_cmd(self, _cmd, args):
        """
        Command completion (i.e. Tab-completion)
        Args:
            _cmd: UNUSED
            args: the (incomplete) command to be queried for completion
        """
        self.protocol.log(LOGGER_LEVEL, "complete_cmd - args: %s" % args)

        @defer.inlineCallbacks
        def retrieve_completion():
            parts = [p for p in args.split(' ') if p]

            if parts[0] == 'help':
                return []

            len_parts = len(parts)
            if args[-1] == ' ':
                len_parts += 1

            if len_parts <= 1:
                completion = [c for c in self.VALID_COMMANDS if c.startswith(args)]
                return [{'data': completion, 'type': 'completion'}]

            if parts[0] not in self.VALID_COMMANDS:
                return [{'data': 'Invalid command: %s' % parts[0], 'type': 'stderr'}]

            shell_command_args = parts + [str(max(1, len_parts - 1))]
            self.protocol.log(LOGGER_LEVEL, "retrieve_completion - "
                                            "shell_command: %s" % shell_command_args)
            try:
                res = yield utils.getProcessOutput(ROS_COMPLETER_SCRIPT_PATH,
                                                   env=os.environ,
                                                   args=shell_command_args)

                res = res.decode('utf-8')

                self.protocol.log(LOGGER_LEVEL, "complete_cmd - "
                                                "Got completion: %s" % res)

                return [{'data': [line for line in res.split('\n') if line], 'type': 'completion'}]
            except Exception as ex:
                self.protocol.log(LOGGER_LEVEL, "complete_cmd - "
                                                "Failed: %s" % ex)
                return [{'data': 'Error retrieving completion: %s' % ex, 'type': 'stderr'}]

        completion_data = yield retrieve_completion()

        self.send_ros_response(data=completion_data, running=False)

    def execute_cmd(self, cmd, args):
        """
        Execute a ROS Command in a shell running in a child process

        Args:
            cmd: The ros* command to execute
            args: the arguments to the command

        """
        self.protocol.log(LOGGER_LEVEL, "execute_cmd - "
                                        "cmd, args: %s, %s" % (cmd, args))

        # https://twistedmatrix.com/documents/current/core/howto/process.html
        self.current_process_transport = reactor.spawnProcess(self.ROSCommandProcessProtocol(self),
                                                              executable=os.path.join(ROS_BIN_DIR,
                                                                                      cmd),
                                                              args=[cmd] + args,
                                                              env=os.environ,
                                                              usePTY=True)

    class ROSCommandProcessProtocol(protocol.ProcessProtocol):

        class Buffer:
            """
            A simple timestamped buffer
            """

            def __init__(self, user_data=None, user_time=None):
                self.user_data = user_data
                self.user_time = user_time

                self.data = None
                self.start_time = None

                self.clear()

            def clear(self):
                self.data = self.user_data if self.user_data is not None else []
                self.start_time = self.user_time if self.user_time is not None else time.time()

            def append(self, item):
                self.data.append(item)

            def __bool__(self):
                return bool(self.data)

            def time_since_clear(self):
                return time.time() - self.start_time

        def __init__(self, command_executor):

            # we must buffer otherwise too frequent messages will overload the client
            self.buffer = self.Buffer()
            self.command_executor = command_executor
            self.command_executor.protocol.log(LOGGER_LEVEL, "ROSCommandProcessProtocol - "
                                                             "init")

        def flush_buffer(self, proc_is_running=True):
            self.command_executor.protocol.log(LOGGER_LEVEL, "flush_buffer - "
                                                             "FLUSH: %s" % self.buffer.data)
            self.command_executor.send_ros_response(data=self.buffer.data,
                                                    running=proc_is_running)
            self.buffer.clear()

        def outReceived(self, data):
            self.received(data, 'out')

        def errReceived(self, data):
            self.received(data, 'err')

        def received(self, data, std_stream):
            decoded_data = data.decode('utf-8')
            self.command_executor.protocol.log(LOGGER_LEVEL, "%sReceived - "
                                                             "%s" % (std_stream, decoded_data))
            # send one line per message for compatibility with ros-terminal.directive.js
            # If too many messages are sent the rendering is very slow
            for line in decoded_data.splitlines():
                self.buffer.append({'data': line, 'type': 'std%s' % std_stream})

            # TODO send multi line messages to improve performances
            # self.buffer.append({'data': data, 'type': 'std%s' % std_stream})

            delta_t = self.buffer.time_since_clear()

            if delta_t > 0.2:
                self.command_executor.protocol.log(LOGGER_LEVEL, "handleReceived - "
                                                                 "DELTA_T: %.2f - "
                                                                 "SEND!" % delta_t)
                self.flush_buffer(proc_is_running=True)
            else:
                self.command_executor.protocol.log(LOGGER_LEVEL, "handleReceived - "
                                                                 "DELTA_T: %.2f - "
                                                                 "BUFFERED!" % delta_t)

        def outConnectionLost(self):
            self.connectionLost("out")

        def errConnectionLost(self):
            self.connectionLost("err")

        def connectionLost(self, std_stream):
            # process std_stream has been closed -> process likely terminated
            self.flush_buffer(proc_is_running=False)

        def processEnded(self, _status):
            self.command_executor.protocol.log(LOGGER_LEVEL, "processEnded")

            # process ended: reset handle in the parent executor and flush buffer
            if self.command_executor.current_process_transport:
                self.flush_buffer(proc_is_running=False)
                self.command_executor.current_process_transport = None
